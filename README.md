# README

My name is Mark Loveless, I've had a few odd titles over the years, but nearly all of them included the word "security". My focus varies, I've done network research, protocol analysis ([example](https://gitlab.com/mloveless/readme/-/blob/main/mitm-netware.md)), reversed engineered various things including binaries (a bit rusty on that I will admit) as well as [phone apps](https://gitlab.com/gitlab-com/gl-security/security-research/phone-application-research) (I've done a lot of those, quite fun), and in general trouble-shot various software applications and even the [odd hardware device](https://duo.com/assets/ebooks/Duo-Labs-Bug-Hunting-Drilling-Into-the-Internet-of-Things-IoT.pdf), including poking and prodding them to find flaws. I've written tools for doing encryption, network scanning, probing systems for security flaws, and many other tasks - but I would be the first to admit my code is not the greatest. I code when I need a tool to do something and I can't find a tool like it.

As a result of my research I've given talks at a lot of security and hacker conferences over the years, and get asked various questions by reporters from different publications.

I should note that I am currently quite happy in my role here at GitLab, and am not interested in changing jobs or doing freelance/consulting work, so no need to ask about those things.

## Work at GitLab

I am a Principal Security Engineer in the Security Department at GitLab. I started here in February 2019 and am quite happy here. Even though I have worked remote since 1999 (my last 5 jobs), this is by far the best place I have ever worked. An all-remote company is the way to go. As an engineer and researcher I tend to do more advanced security analysis and testing, and like the other members of the [Security Research](https://about.gitlab.com/handbook/security/threat-management/security-research/) sub-department where I worked previously to the [Corporate Security](https://handbook.gitlab.com/handbook/security/corporate/) role I'm doing now, I tend to focus on larger and longer-than-a-quarter projects.

I am the designated Cryptographic Officer for GitLab and led development of the [GitLab Cryptographic Standard](https://about.gitlab.com/handbook/security/cryptographic-standard.html) with related work on the FIPS (FIPS 140-2 Attestation included in this [zip file](https://about.gitlab.com/resources/customer-assurance-package/gitlab-cap-current.zip))and [FedRAMP](https://about.gitlab.com/solutions/public-sector/fedramp/) projects.

I'm participating in the [Token Management Working Group](https://about.gitlab.com/company/team/structure/working-groups/token-management/) and led development of the [GitLab Token Management Standard](https://about.gitlab.com/handbook/security/token-management-standard.html).

I get called on for odd jobs and because of my varied past have worked on security incidents, functioned as a SME on various projects in relationship to odd protocols or authentication challenges, worked on GitLab-the-company-specific laptop projects, and typically volunteer first for anything even remotely security-related.

Currently as a Principal I no longer report to a specific sub-department, and am mainly focused on Corporate SaaS and Device Trust engineering projects, although I still dabble in at least five other projects (some in other Security departments, some in other Divisions).

## Work-related Docs

- Update the Handbook from the command line: [update_handbook.md](https://gitlab.com/mloveless/readme/-/blob/main/update_handbook.md)

## Contacting Me

Here are the main places you can find me:

- Here at GitLab: mloveless @ gitlab . com
- Personal email: ml @ markloveless . net
- Hacker email: thegnome @ nmrc . org
- <a rel="me" href="https://rigor-mortis.nrc.org/@simplenomad">Mastodon</a>: [@simplenomad@rigor-mortis.nmrc.org](https://rigor-mortis.nmrc.org/@simplenomad) 
- LinkedIn: [markloveless](https://www.linkedin.com/in/markloveless/) (rarely on, will not connect unless we work together or I know you)
- I'm no longer on Twitter due to the lack of decent moderation and general toxicity.

## Results of my (Recent) Personal Research

- My website: [markloveless.net](http://www.markloveless.net/)

## Older Research

- [CVE-2017-3215 - Milwaukee ONE-KEY bearer token](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-3215)
- [CVE-2017-3214 - Milwaukee ONE-KEY master password](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-3214)
- [CVE-2009-1491 - McAfee GroupShield Bypass](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1491)
- [CVE-2009-1490 - Sendmail Heap Overflow](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2009-1490)
- [CVE-2006-0376 - Windows Wireless Exposure on Laptops](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2006-0376)
- [CVE-2001-1233 - Netware Web Server 5.1 info leak](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2001-1233)
- [CVE-2001-1232 - GroupWise WebAccess Dir Traversal](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2001-1232) 
- [CVE-1999-1132 - Windows NT 4.0 Token Ring DoS](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1999-1132)
- [CVE-1999-1086 - Novell Netware IPX Admin Session Spoof](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1999-1020) (co-author)
- [CVE-1999-1020 - Novell Netware NDS Default Rights](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-1999-1020)

## A Sampling of Conference Talks

- Playlist: [Conference Talks](https://www.youtube.com/playlist?list=PLHQ-nDr38Prhz2d-rODjfT2xWsgyTeXhG)

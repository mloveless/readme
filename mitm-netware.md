## MITM Attacks Against Novell NetWare (old analysis example)

This is from early 2001, and while during the early '00s this was probably one of my favorite projects, virtually no one except the U.S. Government cared about this research. Probably the best thing about it is that the FBI decided I was not someone that should be targeted as a potential criminal, and thought of as an enthusiastic "researcher", at least according to the government officials that contacted me over this effort.

Anyway, here is the original research, bad links and all, from 2001.

-ML

```
MITM Attacks Against Novell NetWare
-----------------------------------

Issue Date: 15Feb2001
Contact: Simple Nomad [thegnome () razor bindview com]
http://razor.bindview.com/publish/advisories/adv_NovellMITM.html


Topic:
------
Man-in-the-middle Attack can reveal password hashes and private keys in
Novell NetWare


Affected Systems:
-----------------
Novell NetWare 5.0, 5.1. Unsure if NDS for other platforms (such as NT, Linux,
or Solaris) are impacted, but since the client software allows connectivity to
NetWare they probably are.


Overview:
---------
Novell has implemented RSA's public/private key technology for encryption and
part of their authentication process. Due to protocol implementation problems,
a man-in-the-middle attack could allow for password hash recovery, and even
a user's RSA private key.


Vendor Contact
--------------
Novell was contacted January 26, 2001. They acknowledged the issues, will
work to improve future products, and added to the mitigation section of this
document.


Impact:
-------
In theory, a network intruder could recover password hashes for cracking and
system access, as well as RSA private keys for authentication.


Details:
--------
Based upon the work originally put forth by Greg Miller [1], plus using
further detailed information regarding the protocol [2], a more complete
analysis of the protocols used during both login and authentication has been
completed.

The "login" protocol is the protocol used during initial authentication to
a Novell Directory Services (NDS) tree. It is a hybrid cryptosystem where a
symmetrical algorithm is used to encrypt data and a public-key algorithm is
used to encrypt a session key. This session key is used to sign packets and
to build a "credential". The "authentication" protocol is used with the
credential to authenticate to additional resources, and is a variation of a
Guillou-Quisquater zero-knowledge identification algorithm. [3]


The Protocols
-------------
In the login protocol, we will assume hash function H is Novell's proprietary
one-way hashing function [4], E(data,key) is encryption, and D(data,key) is
decryption (encryption and decryption are done using RC2 in CBC mode). Novell
has licensed RSA's public/private key technology, and uses the BSAFE SDK that
RSA licenses.

  1. Alice initiates the login sequence, and the server replies with a 4 byte
random value RS1.
  2. Alice generates random values RU1 and RU2, and creates datablock DU1
consisting of RU1 and RU2 (all of these datablocks contain checksums and
usually some padding material to make everything multiples of 8).
  3. Alice retrieves the server's RSA public key KS and computes DU2=E(DU1,KS).
  4. Alice creates datablock DU3 from RS1.
  5. Alice creates datablock DU4 from E(DU3,H(password,UID)).
  6. Alice creates datablock DU5 from random numbers RU3 (which is small) and
RU4 (which is large), and DU4.
  7. Alice computes DU6=E(DU5,RU1).
  8. Alice sends DU2 and DU6 to the server.
  9. The server computes DU1=(DU2,server private key) and extracts RU1 which it
uses to compute DU5=D(DU6,RU1) revealing RU3, RU4, and DU4.
 10. The server decrypts DU3 from DU4, and verifies RS1. The server checks H
against its own copy stored in NDS. If they match then Alice has typed in the
correct password.
 11. The server takes Alice's stored private RSA key KU and creates datablock
DS1 by XORing KU with an equal number of bytes from RU4.
 12. The server builds DS2 from RU3 and DS1, and then builds DS3 from a
timestamp datablock called DS4, and the encryption of DS2 with DU4. The server
sends DS3 to Alice.
 13. Alice decrypts DS2 with DU4, verifies RU3, and XORs DS1 with RU4 to get KU.
 14. Alice builds credential C from DS4, random number RU5, and her full
account name in Unicode. A message digest M is taken of C, and this is
encrypted with KU to get value S. S is used to sign packets.

This does several things. It allows Alice to log in using her password without
the password or the computed hash being transmitted across the wire in clear
text. It allows Alice's RSA private key to be transmitted to Alice for creating
a hash to sign packets without sending it in clear text.

Now to the steps used during authentication to a secondary resource, after
logging in:

  1. Alice sends a request to the server whose resource she wishes to access,
and sends a random value R.
  2. The server generates value R2, and using Alice's public key computes
X=E(K,(R+R2)). This is sent to Alice.
  3. Alice decrypts X and extracts R2. R2 may be related to the time-to-live
of S.
  4. Alice encrypts random values R3, R4, and R5 with her private key to get
E3, E4, and E5 (note here that a Guillou-Quisquater zero-knowledge
identification algorithm would normally use 8 values instead of 3).
  5. Alice creates E6=(E3+E4+E5+S). Alice then computes 16 byte X2=H(E6). She
uses the first three byte pairs of X2 as exponent values EV1, EV2, and EV3 (the
last 10 bytes are ignored).
  6. Alice computes three test values (PKM is Alice's public_key_modulus):

        TV1=(R3*S^EV1) mod PKM
        TV2=(R4*S^EV2) mod PKM
        TV3=(R5*S^EV3) mod PKM

  7. Alice sends E3, E4, E5, TV1, TV2, TV3, and S to the server.
  8. The server creates E6 as Alice did in step 5, recreates X, and gets EV1,
EV2, and EV3. Then the server computes to check the following (assuming PKE is
Alice's Public Key Exponent):

        (TV1^PKE) mod PKM = (E3*(message_digest(C)^EV1)) mod PKM
        (TV2^PKE) mod PKM = (E4*(message_digest(C)^EV2)) mod PKM
        (TV3^PKE) mod PKM = (E5*(message_digest(C)^EV3)) mod PKM

  9. If they work out equal, access to the resource is granted, as it proves
Alice knows the private key.


Attacking the Protocols
-----------------------
There are several scenarios that need to be reviewed in detail. Two are very
similar, and involve a "man-in-the-middle" (MITM) attack against the login
sequence to gain various pieces of material to be used for later attack.

Of course MITM attacks assume that the attacker can sniff the network, process
and resend data to Alice and the server faster than Alice and the server can to
each other. The attacker could conceivably use a combination of arp redirection
and limited denial of service attacks against key network points to help in the
effort to get packets to Alice and the server quickly (even in a switched
environment), although this is probably a bit beyond our discussion. The main
point here is to analyze the possibility of attacking the protocol.

Scenario #1 - Our first MITM scenario is for our attacker Bob to gain access to
the server as Alice during Alice's authentication process:

  1. When Bob sees Alice request a login, Bob also requests a login as Alice
from the server.
  2. The server will generate two random values RS1[a] and RS1[b] -- RS1[a] sent
to Alice and RS1[b] sent to Bob.
  3. When Bob receives RS1[b], he spoofs the server's address and sends RS1[b]
to Alice.
  4. Alice asks the server for its public key KS. Bob asks the server for KS as
well.
  5. Bob sniffs Alice's request for the server's public key and spoofs the reply
with his own public key KB.
  6. Alice sends DU2 and E(DU5,RU1) to the server as normal, but DU2 is
encrypted with Bob's public key.
  7. Bob sniffs this request, performs DU2=E((D(DU2,KB)),KS), and sends DU2 and
Alice's E(DU5,RU1) to the server as his own.
  8. Once all the information is received by the server, Bob's login attempt
will succeed and Alice's will fail, assuming all the packets arrive properly and
Bob gets his packets to the server first.
  9. When the server sends DS3 back to Bob, Bob is logged in as Alice. Bob can
also sign packets, because he has DU4 and can decrypt DS2, and he also has RU4
and can XOR DS1 to get KU.

Scenario #2 - The second MITM attack involves obtaining data for an offline
brute force attack against the recovered material to obtain Alice's password. If
that is Bob's only objective, a variation on the previous MITM attack will work
even better:

  1. When Bob sees Alice request a login, he immediately spoofs a packet as the
server with value RS1 of his own choosing.
  2. When Bob sees Alice request the server's public key, he immediately spoofs
a reply as the server with his own public key.
  3. When Alice sends DU2 and DU6 to the server, Bob sniffs it. Alice's login
attempt will fail, and she will probably just try again.
  4. Bob decrypts DU2, extracts RU1, decrypts DU6, and extracts DU4.
  5. Bob queries the server for Alice's UID (or possibly Bob obtained it via
sniffing during Alice's login attempt).
  6. Bob knows what DU3 is since he provided his own RS1. Therefore Bob can
compute E(DU3,H(password,UID)) using different passwords until he gets a match
with the hash in the recovered DU4. If Alice has chosen a weak password, Bob
will be able to get the password fairly quickly.

Scenario #3 - If Bob at any point obtains X where X=H(Alice's password, Alice's
UID) without knowing what the password actually is, Bob can log in as Alice. X
can be found in the NDS files themselves, and if Bob ever gains access to the
NDS files he can extract X for any account and authenticate as that person:

  1. Bob initiates a login sequence to the server as Alice, and proceeds to
login as normal.
  2. When it comes time to compute DU4, Bob performs E(DU3,X).
  3. The protocol continues, and assuming Alice hasn't changed her password
since Bob obtained X, Bob is logged in as Alice.

This scenario may seem far-fetched as it requires acquisition of the NDS files,
but consider if Bob gains access to the server console, backup tapes, or copies
of backup files left over from certain maintenance operations. In those cases Bob
could very well obtain the NDS files, and X for every account on the server
including Admin.

Scenario #4 - If Bob obtained Alice's private key during Scenario #1 or #3, Bob
can authenticate as Alice to a NetWare resource at a later date, even if she
changes passwords. By sniffing, Bob will know from Alice's last successful login
the value of DS4, RU5, Alice's full account name in Unicode, and can therefore
compute S. By using this and Alice's public key, Bob can compute E3, E4, E5,
TV1, TV2, and TV3 and send that with S as a part of an authentication process
posing as Alice.


Caveats
-------
For the first MITM attack to work, Bob has to be in the right place at the
right time. Bob will have to be able to get information to and from the server
and from Alice faster than they can get it from each other. Possibly by using
a combination of arp tweaks, ICMP quenches, and a network DoS here and there it
might be possible to increase the odds of a successful attack.

I realize that it is entire possible to check Alice's desk for a PostIt note
with the password, which is a much easier attack (and arguably more likely to
succeed) as are many other attack vectors. Nonetheless, it is still a risk.

But the second MITM attack could quite easily work, since Bob can have his
packets for steps 1 and 2 all ready to go, just waiting for Alice to log in.
Then you are solely dependent on the strength of Alice's password for a
security measure.

Even in a switched environment it is still possible to attempt these attacks.
Using arpredirect from the Dsniff package [5] would make this possible, so we
will not be including anything in the Mitigation section that recommends
using a switched network.


Mitigation
----------
Using STATION RESTRICTIONS for everyone is great, although impractical in large
environments. Minimally STATION RESTRICTIONS should be used for those with
access to privileged data, especially system administrators. This and using
strong passwords that expire minimally every 42 days will help prevent offline
attacks. If you really wish to prevent the second MITM attack, lengthen the
passwords being used, make sure they are not dictionary words, and shorten the
number of days between password expirations.

Another mitigation step is to avoid IPX and use IP. The sequence numbering in
SPX/IPX is so unbelievably simple (0 to 255, then repeat as needed) that it is
trivial for hackers to write software that can handle it. Using TCP/IP at least
allows for a level of complexity (although still not impossible to deal with)
that will thwart a lot of spoofing attacks.

It is also possible that alternate backend authentication mechanisms can be
used, or completely different methods of authentication, such as biometrics
or smartcards. Novell has developed NMAS (Novell Modular Authentication
Services) and has partnered with dozens of vendors to provide alternate
methods of authentication. [6]

In summary, a physical network layout that has system administrator traffic
never crossing end user traffic, coupled with STATION RESTRICTIONS, strong
passwords, and even alternate methods of communication should effectively
mitigate crypto attacks.


References:
-----------
[1] Greg Miller's contributions to cryptanalysis of the login process can be
found at http://www.nmrc.org/faqs/netware/nw_sec11.html#11-2. Also, the
following links contain interesting fully commented code that is related:

  http://www.nmrc.org/faqs/netware/a-02.html
  http://www.nmrc.org/faqs/netware/a-03.html
  http://www.nmrc.org/faqs/netware/a-04.html

[2] An anonymous source began floating around details regarding the crypto
being used by Novell during the login and authentication process. Various
people received copies of the documentation, including myself. The analysis in
the documentation was confirmed by packet sniffer.

[3] Hybrid cryptosystems and the Guillou-Quisquater identification algorithm
are discussed in "Applied Cryptography, 2nd Edition" by Bruce Schneier.
ISBN 0-471-11709-9. Published by John Wiley & Sons, Inc.

[4] Novell's hash algorithm was originally published in 1994 in Dr. Dobb's
Journal, and is the algorithm used to generate the password hash. A copy of
the algorithm can be found at http://www.nmrc.org/files/netware/nvpw.zip.

[5] Dsniff by Dug Song can be found at http://www.monkey.org/~dugsong/dsniff.

[6] Details on Novell's NMAS, including a list of NMAS partners, can be found
at http://www.novell.com/products/nmas/.
```

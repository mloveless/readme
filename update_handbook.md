## Update the Handbook

Get the handbook (only have to do this the first time):

```
$ git clone git@gitlab.com:gitlab-com/content-sites/handbook.git
```

Go to the dir:

```
$ cd handbook
```
 
Make sure you're on the master branch, and update your local copy:

```
$ git branch
$ git checkout main
$ git branch
$ git pull origin main
```

Make a new branch to edit:

```
$ git checkout -b mloveless-new-branch-to-edit
```

Edit (add) the file(s)

Check status, and add file(s) before commit:

```
$ git status
$ git add source/whatever/file/you/edited/or/added`
$ git status
```

Commit the file(s) with a message:

```
$ git commit -m "Type in your message about what you did here"
```

Push your branch:

```
$ git push origin mloveless-new-branch-to-edit
```

Follow the link in the cli to complete the merge request.

In the web browser, update title and description, fill out the transparency section, tag appropriate parties, etc.
Make sure "Remove source branch..." and "Squash comments..." are checked, and click on "Submit merge request".
